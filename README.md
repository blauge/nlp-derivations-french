# Dérivateur morphologique

Cet outil permet de générer des flexions d’un mot à partir d’un mot de base.
Il utilise pour ce faire les informations de la base de données [JeuxDeMots](http://www.jeuxdemots.org/) et un ensemble de [règles de dérivations](data/derivations.csv).

## Utilisation

### Dans Eclipse

* Charger le projet.
* Compiler et exécuter la classe `Main.java`.

## Quelques exemples

* jardiner
* tir
* lancement
* constitutionnelle

