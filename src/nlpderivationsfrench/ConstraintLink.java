package nlpderivationsfrench;

import requeterrezo.Mot;

/**
 * Contraint une paire de mots à être liés par une relation.
 * 
 * Permet de s’assurer qu’un mot possède un attribut choisi.
 */
public class ConstraintLink {
	/**
	 * Type de relation devant lier les deux mots.
	 */
	private Relation type;
	
	/**
	 * Reconstruit une contrainte de liaison à partir du format textuel.
	 * Ce texte doit contenir le type de relation (chaîne identifiant
	 * la relation parmi les relations définies dans {@see Relation}).
	 * 
	 * @example "Associated" représente une contrainte demandant que
	 * les deux mots soient associés entre eux.
	 * que le mot possède une relation "IsA" avec la valeur "personne". 
	 * @param input Représentation textuelle de la contrainte.
	 */
	public ConstraintLink(String input) {
		this.type = Relation.valueOf(input);
	}

	/**
	 * Crée une contrainte de liaison.
	 * 
	 * @param type Type de relation devant lier les deux mots.
	 */
	public ConstraintLink(Relation type) {
		super();
		this.type = type;
	}

	/**
	 * Vérifie si un mot satisfait la contrainte de liaison.
	 * 
	 * @param word Mot à vérifier.
	 * @return Vrai si et seulement si le mot satisfait la contrainte.
	 */
	public boolean check(Mot word1, Mot word2) {
		boolean satisfies = word1
				.getRelationsSortantesTypees(this.type.getCode())
				.stream()
				.anyMatch(neighbor -> neighbor.getNoeud().getIdRezo() == word2.getIdRezo());
		
		if (satisfies) {
			return true;
		}
		
		if (this.type.getCodeInverse().isPresent()) {
			// S’il existe une relation symétrique à la relation demandée,
			// on vérifie si par hasard la relation n’est pas renseignée à l’envers
			return word2
					.getRelationsSortantesTypees(this.type.getCodeInverse().get())
					.stream()
					.anyMatch(neighbor -> neighbor.getNoeud().getIdRezo() == word1.getIdRezo());
		}
		
		return false;
	}

	public Relation getType() {
		return this.type;
	}
	
	public String toString() {
		return this.type.toString();
	}
}
