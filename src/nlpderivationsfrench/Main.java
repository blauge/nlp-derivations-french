package nlpderivationsfrench;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try {
			RuleBase rule_base = new RuleBase("data/derivations.csv");

			String word;
			Scanner scanner = new Scanner(System.in);
			while (true) {
				System.out.print("Entrez un mot ou [+] pour s'arrêter : ");
				word = scanner.nextLine();
				if (word.equals("+")) {
					System.out.println("Au revoir !\n");
					break;
				}
				ArrayList<RuleApplicationStatus> results = rule_base.derive(word);

				for (RuleApplicationStatus result : results) {
					System.out.println(result);
				}
				System.out.println();
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
