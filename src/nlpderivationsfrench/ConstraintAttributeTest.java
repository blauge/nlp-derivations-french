package nlpderivationsfrench;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoDump;
import requeterrezo.Resultat;

class ConstraintAttributeTest {
	@Test
	void testConstraintAttributeString() {
		ConstraintAttribute c1 = new ConstraintAttribute("IsA,personne:test");
		assertEquals(c1.getType(), Relation.IsA);
		assertEquals(c1.getValues(), Arrays.asList("personne", "test"));
		
		Exception ex1 = assertThrows(
			IllegalArgumentException.class,
			() -> new ConstraintAttribute("IsA,notvalid,trop de champs")
		);
		assertEquals(
			ex1.getMessage(),
			"Le format textuel pour une contrainte d’attribut "
			+ "doit contenir le type de relation et la valeur "
			+ "attendue, séparés par des espaces. La valeur donnée "
			+ "(IsA,notvalid,trop de champs) contient 3 éléments."
		);
		
		Exception ex2 = assertThrows(
			IllegalArgumentException.class,
			() -> new ConstraintAttribute("TypeInvalide,contenu")
		);
		assertEquals(
			ex2.getMessage(),
			"No enum constant nlpderivationsfrench.Relation.TypeInvalide"
		);
	}

	@Test
	void testConstraintAttributeRelationString() {
		ConstraintAttribute c1 = new ConstraintAttribute(
			Relation.Nature,
			Arrays.asList("Ver", "Inf")
		);
		assertEquals(c1.getType(), Relation.Nature);
		assertEquals(c1.getValues(), Arrays.asList("Ver", "Inf"));
	}

	@Test
	void testCheck() {
		RequeterRezo req = new RequeterRezoDump();

		Resultat res1 = req.requete("tester");
		Mot mot1 = res1.getMot();
		assertNotNull(mot1);
		
		Resultat res2 = req.requete("testé");
		Mot mot2 = res2.getMot();
		assertNotNull(mot2);
		
		Resultat res3 = req.requete("testons");
		Mot mot3 = res3.getMot();
		assertNotNull(mot3);
		
		ConstraintAttribute c1 = new ConstraintAttribute("Nature,Ver:Inf");
		assertTrue(c1.check(mot1));
		assertFalse(c1.check(mot2));
		assertFalse(c1.check(mot3));

		ConstraintAttribute c2 = new ConstraintAttribute("Nature,Ver:PPas");
		assertFalse(c2.check(mot1));
		assertTrue(c2.check(mot2));
		assertFalse(c2.check(mot3));
		
		ConstraintAttribute c3 = new ConstraintAttribute("Nature,Ver:IPre+PL+P1");
		assertFalse(c3.check(mot1));
		assertFalse(c3.check(mot2));
		assertTrue(c3.check(mot3));
	}
}