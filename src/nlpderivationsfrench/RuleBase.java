package nlpderivationsfrench;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoDump;
import requeterrezo.Resultat;

public class RuleBase {

	private RequeterRezo rezo = new RequeterRezoDump();
	private ArrayList<Rule> rules = new ArrayList<Rule>();

	public RuleBase(String file_path) throws FileNotFoundException {
		// Get scanner instance
		Scanner scanner = new Scanner(new File(file_path));

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			line.trim();

			// Ignore les commentaires et les ligne vide
			if (line.equals("") || line.charAt(0) == '#') {
				continue;
			}

			Rule rule = new Rule(rezo, line);
			rules.add(rule);
		}

		// Close the scanner
		scanner.close();
	}

	public ArrayList<RuleApplicationStatus> derive(String word) {
		Queue<String> pending = new LinkedList<>();
		pending.add(word);

		Set<String> seen = new HashSet<>();
		seen.add(word);

		ArrayList<RuleApplicationStatus> results = new ArrayList<>();
		String next;

		while ((next = pending.poll()) != null) {
			Resultat result = rezo.requete(next);
			Mot mot = result.getMot();
			
			if (mot != null) {
				for (Rule rule : this.rules) {
					Rule[] subrules = { rule, rule.invert() };

					for (Rule subrule : subrules) {
						RuleApplicationStatus status = subrule.apply(mot);

						if (status.getWord().isPresent()) {
							String production = status.getWord().get();

							if (!seen.contains(production)) {	
								if (status.valid()) {
									pending.add(production);
								}
								
								results.add(status);
								seen.add(production);
							}
						}
					}
				}
			}
		}
		
		return results;
	}
}
