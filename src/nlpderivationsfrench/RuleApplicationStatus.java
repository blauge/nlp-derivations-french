package nlpderivationsfrench;

import java.util.ArrayList;
import java.util.Optional;

public class RuleApplicationStatus {
	private Optional<String> word = Optional.empty();
	private boolean word_exists = false;

	private ArrayList<ConstraintAttribute> missing_to_attributes = new ArrayList<>();
	private ArrayList<ConstraintAttribute> missing_from_attributes = new ArrayList<>();

	private ArrayList<ConstraintLink> missing_forward_links = new ArrayList<>();
	private ArrayList<ConstraintLink> missing_backward_links = new ArrayList<>();

	public RuleApplicationStatus() {
		super();
	}

	public RuleApplicationStatus(String word, boolean word_exists, ArrayList<ConstraintAttribute> missing_to_attributes,
			ArrayList<ConstraintAttribute> missing_from_attributes, ArrayList<ConstraintLink> missing_forward_links,
			ArrayList<ConstraintLink> missing_backward_links) {
		super();
		this.word = Optional.of(word);
		this.word_exists = word_exists;
		this.missing_to_attributes = missing_to_attributes;
		this.missing_from_attributes = missing_from_attributes;
		this.missing_forward_links = missing_forward_links;
		this.missing_backward_links = missing_backward_links;
	}

	public boolean isApplicable() {
		return this.word.isPresent();
	}

	public boolean valid() {
		return this.isApplicable() && this.word_exists && this.missing_to_attributes.isEmpty()
				&& this.missing_from_attributes.isEmpty() && this.missing_forward_links.isEmpty()
				&& this.missing_backward_links.isEmpty();
	}

	public Optional<String> getWord() {
		return this.word;
	}

	@Override
	public String toString() {
		if (this.word.isEmpty()) {
			return "règle non-applicable";
		}

		String result;

		if (!this.valid()) {
			result = this.word.get() + ", invalide car :\n";
			ArrayList<String> reasons = new ArrayList<>();

			if (!this.word_exists) {
				reasons.add("\t- le mot produit n’existe pas");
			}

			for (ConstraintAttribute constraint : this.missing_from_attributes) {
				reasons.add("\t- l’attribut " + constraint + " est absent sur le mot d’origine");
			}

			for (ConstraintAttribute constraint : this.missing_to_attributes) {
				reasons.add("\t- l’attribut " + constraint + " est absent sur le mot produit");
			}

			for (ConstraintLink constraint : this.missing_forward_links) {
				reasons.add("\t- le lien " + constraint + " est absent entre le mot d’origine et le mot produit");
			}

			for (ConstraintLink constraint : this.missing_backward_links) {
				reasons.add("\t- le lien " + constraint + " est absent entre le mot produit et le mot d’origine");
			}

			result += String.join("\n", reasons);
		} else {
			result = "\u001B[32m" + this.word.get() + "\u001B[0m"; 
		}

		return result;
	}
}
