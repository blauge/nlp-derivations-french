package nlpderivationsfrench;

import java.util.Optional;

/**
 * Types de relations possibles de rezoJDM.
 * 
 * @see <a href="http://www.jeuxdemots.org/jdm-about-detail-relations.php">Documentation des types de relations</a>
 */
public enum Relation {
	/**
	 * Termes les plus étroitement associés au mot.
	 */
	Associated(0, 0),
	
	/**
	 * Domaines relatifs au mot.
	 */
	Domain(3, 27),
	
	/**
	 * Nature grammaticale d’un mot (nom, verbe, adjectif, adverbe, …).
	 */
	Nature(4),
	
	/**
	 * Génériques/hyperonymes du terme.
	 */
	IsA(6),
	
	/**
	 * Antonyme d’un mot.
	 */
	Antonym(7),
	
	/**
	 * Entité effectuant l’action d’un verbe.
	 */
	Agent(13, 24),
	
	/**
	 * Entité subissant l’action d’un verbe.
	 */
	Patient(14, 26),
	
	/**
	 * Informations lexicales diverses.
	 */
	Data(18),
	
	/**
	 * Lemme d'un mot.
	 */
	Lemma(19),
	
	/**
	 * Adjectif de potentialité d’un verbe.
	 */
	VerbeAdjectif(44, 43),
	
	/**
	 * Lien du masculin vers le féminin d’un mot.
	 */
	ToFem(60, 59);
	
	/**
	 * Code identifiant le type de relation dans rezoJDM.
	 */
	private int code;
	
	/**
	 * Code identifiant le type inverse de relation dans rezoJDM
	 * si une telle relation existe.
	 */
	private Optional<Integer> code_inverse = Optional.empty();
	
	Relation(int code) {
		this.code = code;
	}
	
	Relation(int code, int code_inverse) {
		this.code = code;
		this.code_inverse = Optional.of(code_inverse);
	}
	
	public int getCode() {
		return this.code;
	}
	
	public Optional<Integer> getCodeInverse() {
		return this.code_inverse;
	}
}
