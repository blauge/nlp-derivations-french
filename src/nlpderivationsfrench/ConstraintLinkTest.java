package nlpderivationsfrench;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoDump;
import requeterrezo.Resultat;

class ConstraintLinkTest {
	@Test
	void testConstraintLinkString() {
		ConstraintLink c1 = new ConstraintLink("Associated");
		assertEquals(c1.getType(), Relation.Associated);
		
		Exception ex1 = assertThrows(
			IllegalArgumentException.class,
			() -> new ConstraintLink("CeTypeNEstPasValide")
		);
		assertEquals(
			ex1.getMessage(),
			"No enum constant nlpderivationsfrench.Relation.CeTypeNEstPasValide"
		);
	}

	@Test
	void testConstraintLinkRelation() {
		ConstraintLink c1 = new ConstraintLink(Relation.IsA);
		assertEquals(c1.getType(), Relation.IsA);
	}

	@Test
	void testCheck() {
		RequeterRezo req = new RequeterRezoDump();
		
		Resultat res1 = req.requete("tester");
		Mot mot1 = res1.getMot();
		assertNotNull(mot1);

		Resultat res2 = req.requete("vérifier");
		Mot mot2 = res2.getMot();
		assertNotNull(mot2);
		
		Resultat res3 = req.requete("testé");
		Mot mot3 = res3.getMot();
		assertNotNull(mot3);
		
		Resultat res4 = req.requete("faisabilité");
		Mot mot4 = res4.getMot();
		assertNotNull(mot4);
		
		Resultat res5 = req.requete("juge");
		Mot mot5 = res5.getMot();
		assertNotNull(mot5);
		
		Resultat res6 = req.requete("magistrat");
		Mot mot6 = res6.getMot();
		assertNotNull(mot6);
		
		ConstraintLink c1 = new ConstraintLink("Associated");
		assertTrue(c1.check(mot1, mot2));
		assertTrue(c1.check(mot1, mot3));
		assertFalse(c1.check(mot1, mot4));
		assertTrue(c1.check(mot2, mot1));
		assertFalse(c1.check(mot2, mot3));
		assertFalse(c1.check(mot2, mot4));
		assertTrue(c1.check(mot3, mot1));
		assertFalse(c1.check(mot3, mot2));
		assertFalse(c1.check(mot3, mot4));
		assertFalse(c1.check(mot4, mot1));
		assertFalse(c1.check(mot4, mot2));
		assertFalse(c1.check(mot4, mot3));

		ConstraintLink c2 = new ConstraintLink("Patient");
		assertFalse(c2.check(mot1, mot2));
		assertFalse(c2.check(mot1, mot3));
		assertTrue(c2.check(mot1, mot4));
		assertFalse(c2.check(mot2, mot1));
		assertFalse(c2.check(mot2, mot3));
		assertTrue(c2.check(mot2, mot4));
		assertFalse(c2.check(mot3, mot1));
		assertFalse(c2.check(mot3, mot2));
		assertFalse(c2.check(mot3, mot4));
		assertFalse(c2.check(mot4, mot1));
		assertFalse(c2.check(mot4, mot2));
		assertFalse(c2.check(mot4, mot3));
		
		ConstraintLink c3 = new ConstraintLink("IsA");
		assertTrue(c3.check(mot5, mot6));
		assertFalse(c3.check(mot6, mot5));
	}
}
