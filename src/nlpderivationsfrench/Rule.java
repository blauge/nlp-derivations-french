package nlpderivationsfrench;

import java.util.ArrayList;
import java.util.Optional;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.Resultat;

/**
 * règle d'inférence d'un mot
 * 
 * permet de vérifier et appliqué une règle de dérivation
 */
public class Rule {

	private RequeterRezo requeter;

	private String name;

	private String from;
	private String to;
	
	private ArrayList<ConstraintAttribute> from_attributes;
	private ArrayList<ConstraintAttribute> to_attributes;

	private ArrayList<ConstraintLink> backward_links;
	private ArrayList<ConstraintLink> forward_links;

	public Rule(
		RequeterRezo requeter,
		String name,
		String from,
		String to,
		ArrayList<ConstraintAttribute> from_attributes,
		ArrayList<ConstraintAttribute> to_attributes,
		ArrayList<ConstraintLink> backward_links,
		ArrayList<ConstraintLink> forward_links
	) {
		super();
		this.requeter = requeter;
		this.name = name;
		this.from = from;
		this.to = to;
		this.from_attributes = from_attributes;
		this.to_attributes = to_attributes;
		this.backward_links = backward_links;
		this.forward_links = forward_links;
	}

	public Rule(RequeterRezo requeter, String regle) {
		// Initialisation des attributs
		this.requeter = requeter;
		this.from_attributes = new ArrayList<>();
		this.to_attributes = new ArrayList<>();
		this.backward_links = new ArrayList<>();
		this.forward_links = new ArrayList<>();

		// analyseur de la règle
		String[] attributs_rule = regle.split(";");

		// name
		this.name = attributs_rule[0];

		// from
		this.from = attributs_rule[1];

		// to
		this.to = attributs_rule[2];

		// parssage des contraintes
		for (int i = 3; i < attributs_rule.length; i++) {
			String[] attributs_constraint = attributs_rule[i].split(",");

			// constrainte attribut
			if (attributs_constraint[0].equals("attr")) {
				String target = attributs_constraint[1];

				String input = attributs_constraint[2] + ',' + attributs_constraint[3];

				if (target.equals("from")) {
					this.from_attributes.add(new ConstraintAttribute(input));
				} else if (target.equals("to")) {
					this.to_attributes.add(new ConstraintAttribute(input));
				}
			}
			// constrainte lien
			else if (attributs_constraint[0].equals("link")) {
				String direction = attributs_constraint[1];
				String input = attributs_constraint[2];

				if (direction.equals("->")) {
					this.forward_links.add(new ConstraintLink(input));
				} else if (direction.equals("<-")) {
					this.backward_links.add(new ConstraintLink(input));
				}
			}
		}
	}

	/**
	 * Applique la règle.
	 * 
	 * @param word mot à dériver
	 * @return résultat de l’application
	 */
	public RuleApplicationStatus apply(Mot mot) {
		// Test si la règle match avec le mot
		Optional<String> optional_root = this.getRoot(mot.getMotFormate());

		if (optional_root.isEmpty()) {
			return new RuleApplicationStatus();
		}

		String root = optional_root.get();
		
		// Statut de l’application de la règle
		ArrayList<ConstraintAttribute> missing_from_attributes = new ArrayList<>();
		ArrayList<ConstraintAttribute> missing_to_attributes = new ArrayList<>();

		ArrayList<ConstraintLink> missing_forward_links = new ArrayList<>();
		ArrayList<ConstraintLink> missing_backward_links = new ArrayList<>();

		// Vérifie les contraintes d’attributs sur le mot d’origine
		for (ConstraintAttribute constraint : this.from_attributes) {
			if (!constraint.check(mot)) {
				missing_from_attributes.add(constraint);
			}
		}

		// Reconstitue le mot résultant
		String result_word = this.rebuildWord(root);

		Resultat result_requete = this.requeter.requete(result_word);
		Mot result_mot = result_requete.getMot();

		if (result_mot == null) {
			return new RuleApplicationStatus(
				result_word,
				/* word_exists = */ false,
				missing_to_attributes,
				missing_from_attributes,
				missing_forward_links,
				missing_backward_links
			);
		}

		// Vérifie les contraintes d’attributs sur le mot dérivé
		for (ConstraintAttribute constraint : this.to_attributes) {
			if (!constraint.check(result_mot)) {
				missing_to_attributes.add(constraint);
			}
		}

		// Vérifie les contraintes de lien du mot d’origine vers le mot produit
		for (ConstraintLink constraint : this.forward_links) {
			if (!constraint.check(mot, result_mot)) {
				missing_forward_links.add(constraint);
			}
		}

		// Vérifie les contraintes de lien du mot produit vers le mot d’origine
		for (ConstraintLink constraint : this.backward_links) {
			if (!constraint.check(result_mot, mot)) {
				missing_backward_links.add(constraint);
			}
		}

		return new RuleApplicationStatus(
			result_word,
			/* word_exists = */ true,
			missing_to_attributes,
			missing_from_attributes,
			missing_forward_links,
			missing_backward_links
		);
	}

	/**
	 * extrait le préfixe du mot si la règle correspond
	 * 
	 * @param word mot duquel on veut extraire le préfixe
	 * @return préfice du mot si il a la bonne terminaison
	 */
	private Optional<String> getRoot(String word) {
		String[] parts = this.from.split("\\*", -1);
		
		if (!word.startsWith(parts[0]) || !word.endsWith(parts[1])) {
			return Optional.empty();
		}
		
		return Optional.of(word.substring(
			parts[0].length(),
			word.length() - parts[1].length()
		));
	}

	/**
	 * Reconstitue un mot à partir d’une racine.
	 * 
	 * @param root a reconstitué
	 * @return mot reconstitué avec le résultat de la règle
	 */
	private String rebuildWord(String root) {
		String[] parts = this.to.split("\\*", -1);
		return parts[0] + root + parts[1];
	}

	/**
	 * Crée la règle inverse de la règle actuelle.
	 * 
	 * @return Crée une règle dérivant dans le sens opposé.
	 */
	public Rule invert() {
		return new Rule(
			this.requeter,
			this.name,
			this.to,
			this.from,
			this.to_attributes,
			this.from_attributes,
			this.forward_links,
			this.backward_links
		);
	}
	
	/**
	 * retourne le nom de la règle
	 * 
	 * @return nom de la règle
	 */
	@Override
	public String toString() {
		return this.name;
	}

}
