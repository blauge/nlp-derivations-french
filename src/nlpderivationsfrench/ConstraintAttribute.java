package nlpderivationsfrench;

import java.util.Arrays;
import java.util.List;

import requeterrezo.Mot;

/**
 * Contraint un mot à disposer d’une relation ayant une valeur fixée.
 * 
 * Permet de s’assurer qu’un mot possède un attribut choisi.
 */
public class ConstraintAttribute {
	/**
	 * Type de relation devant être associée au mot.
	 */
	private Relation type;
	
	/**
	 * Valeur attendue pour la relation.
	 */
	private List<String> values;
	
	/**
	 * Reconstruit une contrainte d’attribut à partir du format textuel.
	 * Ce texte doit contenir les deux informations suivantes :
	 * 
	 * * type de relation (chaîne identifiant la relation parmi
	 * 	 les relations définies dans {@see Relation} ;
	 * * valeur attendue pour la relation ;
	 * 
	 * séparées par une virgule.
	 * 
	 * @example "IsA,personne" représente une contrainte demandant
	 * que le mot possède une relation "IsA" avec la valeur "personne". 
	 * @param input Représentation textuelle de la contrainte.
	 */
	public ConstraintAttribute(String input) {
		String[] items = input.split(",");
		
		if (items.length != 2) {
			throw new IllegalArgumentException(String.format(
				"Le format textuel pour une contrainte d’attribut "
				+ "doit contenir le type de relation et la valeur "
				+ "attendue, séparés par des espaces. La valeur donnée "
				+ "(%s) contient %d éléments.",
				String.join(",", input),
				items.length));
		}
		
		this.type = Relation.valueOf(items[0]);
		this.values = Arrays.asList(items[1].split(":"));
	}

	/**
	 * Crée une contrainte d’attribut.
	 * 
	 * @param type Type de relation devant être associée au mot.
	 * @param values Valeur attendue pour la relation.
	 */
	public ConstraintAttribute(Relation type, List<String> values) {
		super();
		this.type = type;
		this.values = values;
	}

	/**
	 * Vérifie si un mot satisfait la contrainte d’attribut.
	 * 
	 * @param word Mot à vérifier.
	 * @return Vrai si et seulement si le mot satisfait la contrainte.
	 */
	public boolean check(Mot word) {
		return word
				.getRelationsSortantesTypees(this.type.getCode())
				.stream()
				.anyMatch(neighbor -> {
					List<String> parts = Arrays.asList(neighbor.getNom().split(":"));
					return parts.containsAll(this.values);
				});
	}

	public Relation getType() {
		return this.type;
	}

	public List<String> getValues() {
		return this.values;
	}
	
	public String toString() {
		return this.type + " = " + this.values;
	}
}
